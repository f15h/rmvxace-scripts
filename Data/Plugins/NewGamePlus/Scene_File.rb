class Scene_Load < Scene_File

  def on_savefile_ok
    super

    if !@savefile_windows[@index].new_game_plus_save? and DataManager.load_game(@index)
      on_load_success
      return nil
    end

    if @savefile_windows[@index].new_game_plus_save? and DataManager.setup_new_game_plus(@index)
      on_load_success
      return nil
    end

    Sound.play_buzzer
  end
end