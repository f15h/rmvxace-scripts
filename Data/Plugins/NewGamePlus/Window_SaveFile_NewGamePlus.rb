class Window_SaveFile_NewGamePlus < Window_SaveFile
    def initialize(height, index)
      super(height, index)

    end
  def new_game_plus_save_text
    DF::NewGamePlus::Constants::NG_TEXT_SAVE_TEXT
  end

  def refresh
    contents.clear
    if new_game_plus_save?
      draw_new_game_plus_context({ enabled: true })
      return nil
    end
    draw_save_context({ enabled: false })
  end

    def update_tone
      puts "update tone"
      self.tone.set(DF::NewGamePlus::Constants::NG_WINDOW_TONE)
    end

end