module DF
  module NewGamePlus
    module Selectors

      def self.it_new_game_plus_save?(headers)
        if save_file_exists?(headers)
          return headers[:switches][DF::NewGamePlus::Constants::NG_SWITCH_NUMBER]
        end
        return false
      end

      def self.save_file_exists?(headers)
        return false if headers.nil?
        return false if headers[:switches].nil?
        return true
      end

    end
  end
end
