module DataManager

  def self.setup_new_game_plus(index)
    create_new_game_plus_objects(index)
    $game_map.setup($data_system.start_map_id)
    $game_player.moveto($data_system.start_x, $data_system.start_y)
    $game_player.refresh
    Graphics.frame_count = 0
  end

  def self.create_new_game_plus_objects(index)
    load_game_without_rescue(index)
    new_game_plus_reset_switches
    new_game_plus_reset_variables
    new_game_plus_reset_self_switches
    new_game_plus_reset_actors
    new_game_plus_reset_party
  end

  def self.new_game_plus_reset_switches
    $game_switches = Game_Switches.new
    $game_switches[DF::NewGamePlus::Constants::NG_SWITCH_NUMBER] = true
  end

  def self.new_game_plus_reset_self_switches
    $game_self_switches = Game_SelfSwitches.new
  end

  def self.new_game_plus_reset_variables
    (0...$data_system.variables.size).each { |i|
      $game_variables[i] = 0
    }
  end

  def self.new_game_plus_reset_actors
    $game_actors = Game_Actors.new
  end

  def self.new_game_plus_reset_party
    $game_party = Game_Party.new
    $game_party.setup_starting_members
  end

  def self.make_save_header
    header = {}
    header[:characters]    = $game_party.characters_for_savefile
    header[:playtime_s]    = $game_system.playtime_s
    header[:system]        = Marshal.load(Marshal.dump($game_system))
    header[:timer]         = Marshal.load(Marshal.dump($game_timer))
    header[:message]       = Marshal.load(Marshal.dump($game_message))
    header[:switches]      = Marshal.load(Marshal.dump($game_switches))
    header[:variables]     = Marshal.load(Marshal.dump($game_variables))
    header[:self_switches] = Marshal.load(Marshal.dump($game_self_switches))
    header[:actors]        = Marshal.load(Marshal.dump($game_actors))
    header[:party]         = Marshal.load(Marshal.dump($game_party))
    header[:troop]         = Marshal.load(Marshal.dump($game_troop))
    header[:map]           = Marshal.load(Marshal.dump($game_map))
    header[:player]        = Marshal.load(Marshal.dump($game_player))
    header
  end

  def self.new_game_plus_saves_exists?
    Dir.glob('Save*.rvdata2').any? { |filename|
      headers = load_header_without_rescue_by_name(filename)
      DF::NewGamePlus::Selectors::it_new_game_plus_save?(headers)
    }
  end


  def self.load_header_without_rescue_by_name(filename)
    File.open(filename, 'rb') do |file|
      return Marshal.load(file)
    end
    nil
  end
end
