# DEBS

- gnu make (http://gnuwin32.sourceforge.net/downlinks/make.php )
- ruby ( http://www.ruby-lang.org - pik works well for windows)
- rvpacker (https://rubygems.org/gems/rvpacker - gem install rvpacker)

# PRECONDITION

- download and install ruby ruby ( http://www.ruby-lang.org ) 
- download and install gnu make ( http://gnuwin32.sourceforge.net/downlinks/make.php )
- add to PATH variable make
- install rvpacker use gem `gem install rvpacker`


# USAGE

`make unpack`

# KNOW ISSUES

## cannot load such file -- scanf (LoadError)
- install scanf library for rvpacker `gem install scanf`
